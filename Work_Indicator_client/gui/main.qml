import QtQuick 2.12
import QtQuick.Controls 2.14
import QtQuick.Window 2.13
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Styles 1.4

import Qt.labs.platform 1.1

import BluetoothDevices 1.0

ApplicationWindow {
    id: window
    width: 640
    height: 480
    minimumHeight:  480
    minimumWidth: 640

    title: qsTr("Home Office Indicator")

    property bool materialThemeDark: optionsView.darkTheme
    Material.theme: materialThemeDark ? Material.Dark : Material.Light

    OptionsView {
        id: optionsView
    }

    SystemTrayIcon {
        visible: true
        icon.source: "qrc:/images/led-light.png"

        onActivated: {
                available: true
                show()
                raise()
             }

        menu: Menu {
            id: trayMenu
            MenuItem {
                text: qsTr("Options")
                onTriggered: {
                    window.show()
                    window.raise()
                    window.requestActivate()
                }
            }
            MenuSeparator {}
            MenuItem {
                enabled: btDeviceManager.connectedState ? true : false
                iconSource: "qrc:/images/red.png"
                text: qsTr("Don't disturb")
                onTriggered: {
                    btDeviceManager.setState(LedService.E_LED_STATE_RED);
                }
            }
            MenuItem {
                enabled: btDeviceManager.connectedState ? true : false
                iconSource: "qrc:/images/home-office.svg"
                text: qsTr("Working")
                onTriggered: {
                    btDeviceManager.setState(LedService.E_LED_STATE_YELLOW);
                }
            }
            MenuItem {
                enabled: btDeviceManager.connectedState ? true : false
                iconSource: "qrc:/images/free.ico"
                text: qsTr("Free")
                onTriggered: {
                    btDeviceManager.setState(LedService.E_LED_STATE_GREEN);
                }
            }
            MenuItem {
                enabled: btDeviceManager.connectedState ? true : false
                text: qsTr("Off")
                iconSource: "qrc:/images/relaxing.svg"
                onTriggered: {
                    btDeviceManager.setState(LedService.E_LED_STATE_OFF);
                }
            }
            MenuSeparator {}
            MenuItem {
                text: qsTr("About")
            }
            MenuItem {
                text: qsTr("Quit")
                onTriggered: Qt.quit()
            }
        }
    }

    Component.onCompleted: {
        window.hide()
    }

    onClosing: {
        close.accepted = false
        window.hide()
    }
}
