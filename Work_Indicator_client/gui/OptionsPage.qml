import QtQuick 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Page {
    id: optionsPage
    Layout.fillHeight: parent.height
    Layout.fillWidth: parent.width

    property alias switchTheme: switchTheme.checked

    GroupBox {
        id: themeOptions
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 30
        title: qsTr("Theme")
        
        RowLayout {
            id: rowLayout
            anchors.fill: parent
            
            Label {
                id: themeLabel
                text: qsTr("Thame Dark/Light")
            }
            
            Switch {
                id: switchTheme
                text: qsTr("Theme")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.fillWidth: true
            }
            
        }
    }
}
