﻿import QtQuick 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

import BluetoothDevices 1.0

Page {
    id: connectionPage

    Layout.fillHeight: parent.height
    Layout.fillWidth: parent.width

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent
        
        GroupBox {
            id: statusBox
            Layout.fillHeight: true
            Layout.fillWidth: false
            title: qsTr("Connection Status")
            
            ColumnLayout {
                id: columnLayout2
                Text {
                    id: connectionLabel
                }
            }

            states: [
                State {
                 name: "connected";
                 when: btDeviceManager.connectedState === true;
                 extend: "normal"
                 PropertyChanges {
                     target: connectionLabel
                     text: qsTr("Connected")
                 }
             },
             State {
                 name: "disconnected";
                 when: btDeviceManager.connectedState === false;
                 extend: "normal"
                 PropertyChanges {
                     target: connectionLabel
                     text: qsTr("Disconnected")
                 }
             }
            ]
        }

        GroupBox {
            id: bluetoothDevicesBox
            Layout.fillHeight: true
            Layout.fillWidth: true
            title: qsTr("Bluetooth Devices")

            RowLayout {
                id: rowLayout
                anchors.fill: parent

                Frame {
                    id: frame
                    width: 200
                    height: 200
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    ListView {
                        id: listView
                        visible: true
                        anchors.fill: parent
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        clip: true
                        focus: true

                        model: BluetoothDeviceModel {
                            id: btModel
                        }

                        delegate:
                            Item {
                            height: 40
                            width: listView.width

                            property variant myData: model

                            Column {
                                RowLayout {
                                    Text {
                                        text: '<b>Name:</b> ' + deviceName
                                        Layout.minimumWidth: 150
                                    }
                                    Text {
                                        text: '<b>RSSI:</b> ' + deviceRssi
                                    }
                                }

                                Text { text: '<b>UUID:</b> ' + deviceUuid }
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: listView.currentIndex = index
                            }
                        }
                        highlight: Rectangle {
                            color: "lightsteelblue";
                            radius: 5
                        }
                    }
                }

                ColumnLayout {
                    id: columnLayout
                    width: 100
                    height: 100
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

                    Button {
                        id: scanButton
                        text: qsTr("Scan for devices")

                        onPressed: {
                            btModel.scanDevices()
                        }
                    }

                    Button {
                        id: connectButton
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                       states: [
                           State {
                            name: "connected";
                            when: btDeviceManager.connectedState === true;
                            extend: "normal"
                            PropertyChanges {
                                target: connectButton
                                enabled: true
                                text: qsTr("Disconnect from device")
                                onPressed: {
                                    btDeviceManager.disconnectFromDevice();
                                }
                            }
                        },
                        State {
                            name: "disconnected";
                            when: btDeviceManager.connectedState === false;
                            extend: "normal"
                            PropertyChanges {
                                target: connectButton
                                enabled: true
                                text: "Connect to device"
                                onPressed: {
                                    btDeviceManager.connectToDevice(listView.currentItem.myData.device);
                                }
                            }
                        }
                       ]
                    }

                    Button {
                        id: cancelButton
                        text: qsTr("Cancel")
                        enabled: false
                        onPressed: {
                            btModel.cancelDeviceScan()
                        }
                    }

                    BusyIndicator {
                        id: loadingDevicesIndycator
                        Layout.fillHeight: true
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        running: false
                    }
                }

            }
        }
    }

    states: [
        State {
            name: "normal"; when: btModel.scaningState == false;
            PropertyChanges {
                target: scanButton;
                enabled: true
            }
            PropertyChanges {
                target: connectButton
                enabled: true
            }
            PropertyChanges {
                target: cancelButton
                enabled: false
            }
            PropertyChanges {
                target: listView;
                enabled: true
            }
            PropertyChanges {
                target: loadingDevicesIndycator;
                running: false
            }
        },

        State {
            name: "scanning"; when: btModel.scaningState == true;
            PropertyChanges {
                target: scanButton;
                enabled: false
            }
            PropertyChanges {
                target: connectButton
                enabled: false
            }
            PropertyChanges {
                target: cancelButton
                enabled: true
            }
            PropertyChanges {
                target: listView;
                enabled: false
            }
            PropertyChanges {
                target: loadingDevicesIndycator;
                running: true
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:8;anchors_height:160;anchors_width:110;anchors_x:"-604";anchors_y:"-228"}
D{i:6;anchors_height:100;anchors_width:100}
}
##^##*/
