import QtQuick 2.0
import QtQuick.Controls.Material 2.12

Item {
    property bool materialThemeDark: true

    id: background
    anchors.fill: parent

    Material.theme: materialThemeDark ? Material.Dark : Material.Light
}
