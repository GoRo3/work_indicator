import QtQuick 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Item {
    anchors.fill: parent

    property alias darkTheme: optionsPage.switchTheme

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        TabBar {
            id: tabBar
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true

            currentIndex: swipeView.currentIndex

            TabButton {
                id: connection
                text: qsTr("Connection")
            }

            TabButton {
                id: options
                text: qsTr("Options")
                anchors.top: parent.top
            }
        }

        SwipeView {
            id: swipeView
            Layout.fillHeight: true
            Layout.fillWidth: true
            currentIndex: tabBar.currentIndex

            ConnectionPage {
                id: connectionPage
            }
            OptionsPage {
                id: optionsPage
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:7;anchors_height:100;anchors_width:100}
D{i:6;anchors_height:200;anchors_width:200}
}
##^##*/
