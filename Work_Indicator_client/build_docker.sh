#!/bin/sh
IMAGE_NAME=goro3/qt_ubuntu:latest

sudo docker run -it --rm -v $(pwd):/Qt_project $IMAGE_NAME /bin/bash -c "mkdir -p build && cd build && cmake -DENABLE_TESTS=YES .. && make -j6 && make install && cpack"  