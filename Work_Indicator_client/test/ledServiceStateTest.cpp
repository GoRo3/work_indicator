#include <QObject>
#include <QTest>

#include <QByteArray>

#include "ledservicestate.hpp"

class LedServiceStateTest: public QObject
{
    Q_OBJECT
private:
    LedServiceState ledstate1 { LedServiceState::LedState_t::E_LED_STATE_OFF };

private slots:
    void initTestCase()
    {
        qDebug("Seting Up service state enviroment");
    }

    void compareOperatorTest()
    {
        LedServiceState ledstate2(LedServiceState::LedState_t::E_LED_STATE_OFF);

        QVERIFY(ledstate1 == ledstate2);
    }

    void negCompareOperatorTest()
    {
        LedServiceState ledstate2(LedServiceState::LedState_t::E_LED_STATE_MAX);

        QVERIFY(ledstate1 != ledstate2);
    }

    void setledStatefromByteArrayTest()
    {
        QByteArray array;
        array.resize(1);
        array[0] = 1;

        QVERIFY2(ledstate1.setState(array), "Check if array wil set state");
    }

    void toBigByteArrayTest()
    {
        QByteArray array;
        array.resize(10);

        QCOMPARE(ledstate1.setState(array), false);
    }

    void wrongNumberfromByteArrayTest()
    {
        QByteArray array;
        array.resize(1);
        array[0] = 56;

        QCOMPARE(ledstate1.setState(array), false);
    }

    void negNumberfromByteArrayTest()
    {
        QByteArray array;
        array.resize(1);
        array[0] = -1;

        QCOMPARE(ledstate1.setState(array), false);
    }

    void cleanupTestCase()
    {
    }
};

QTEST_MAIN(LedServiceStateTest)
#include "ledServiceStateTest.moc"
