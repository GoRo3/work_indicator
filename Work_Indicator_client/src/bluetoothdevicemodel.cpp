#include "bluetoothdevicemodel.hpp"

#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QBluetoothUuid>
#include <QDebug>
#include <QMetaEnum>
#include <QString>

#include <algorithm>
#include <memory>
#include <mutex>

#include "btdevice.hpp"
#include "btindicator.hpp"

static constexpr int DEFAULT_DISCOVERY_TIME_MS = 10000;

// TODO (GORO3) - Make BtDevice* a smart pointer.

BluetoothDeviceModel::BluetoothDeviceModel(QObject *parent) : QAbstractListModel(parent),
                                                              m_deviceDiscoveryAgent(new QBluetoothDeviceDiscoveryAgent(this))
{
    m_deviceDiscoveryAgent->setLowEnergyDiscoveryTimeout(DEFAULT_DISCOVERY_TIME_MS);
//    connect(m_deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, [&](const auto a_info){
//    });

    connect(m_deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished, this, &BluetoothDeviceModel::scanFinished);
    connect(m_deviceDiscoveryAgent, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error),
            this, &BluetoothDeviceModel::deviceScanError);
    connect(m_deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::canceled, [&](){
            m_scaningState = false;
            emit scaningStateChanged();
    });
}

int BluetoothDeviceModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid() || m_devices.empty()) {
        return 0;
    }

    return m_devices.size();
}

QVariant BluetoothDeviceModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const auto btDevice = m_devices.at(index.row());

    switch (role) {
    case E_DeviceAddres: {
        return QVariant(btDevice->deviceAddress());
    }
    case E_DeviceName: {
        return QVariant(btDevice->deviceName());
    }
    case E_DeviceUUID: {
        return QVariant(btDevice->deviceUuid());
    }
    case E_Device: {
        return QVariant(QVariant::fromValue(btDevice));
    }
    case E_DeviceRssi: {
        return QVariant(btDevice->deviceRssi());
    }
    case E_DeviceIsCompatible: {
        return QVariant(btDevice->isCompatible());
    }
    default: {
        return QVariant();
    }
    }
}

QHash<int, QByteArray> BluetoothDeviceModel::roleNames() const
{
    QHash<int ,QByteArray> roles;
    roles[E_DeviceAddres] = "deviceAddress";
    roles[E_DeviceName] = "deviceName";
    roles[E_DeviceUUID] = "deviceUuid";
    roles[E_Device] = "device";
    roles[E_DeviceRssi] = "deviceRssi";
    roles[E_DeviceIsCompatible] = "isCompatible";

    return roles;
}

Qt::ItemFlags BluetoothDeviceModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsSelectable;
}

void BluetoothDeviceModel::scanDevices()
{
    if (!m_devices.empty()) {
        emit layoutAboutToBeChanged();
        for (auto &device: m_devices) {
            free(device);
        }
        m_devices.clear();

        emit layoutChanged();
    }

    scanForBtDevices();
}

void BluetoothDeviceModel::scanForBtDevices()
{
    m_deviceDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);

    m_scaningState = true;
    emit scaningStateChanged();
}

bool BluetoothDeviceModel::checkIfDeviceisKnown(const QBluetoothDeviceInfo &a_info)
{
    const auto serviceList = a_info.serviceUuids();
    QBluetoothUuid tempUUID((QString(IndicatorServices::LED_SERVICE)));

    if (std::any_of(serviceList.begin(), serviceList.end(), [tempUUID](const auto &uuid){
        return tempUUID == uuid;
        })) {
        return true;
    }

    return false;
}

bool BluetoothDeviceModel::scaningState() const
{
    return m_scaningState;
}

void BluetoothDeviceModel::setScaningState(bool scaningState)
{
    m_scaningState = scaningState;
}

void BluetoothDeviceModel::addDeviceToList(const QBluetoothDeviceInfo &a_info)
{
    if (a_info.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration) {

        BtDevice *device = nullptr;

        if (checkIfDeviceisKnown(a_info)) {
            device = new BtIndicator(a_info);
            qDebug() << a_info.name();
        } else {
            device = new BtDevice(a_info);
        }

        const auto index = m_devices.size();
        emit beginInsertRows(QModelIndex(), index, index);
        m_devices.push_back(device);
        endInsertRows();
    }
}

void BluetoothDeviceModel::scanFinished()
{
    qInfo() << "Scan finished, found BT devices";
    const auto devicesList = m_deviceDiscoveryAgent->discoveredDevices();

    for (const auto &deviceInfo : devicesList) {
        addDeviceToList(deviceInfo);
    }

    m_scaningState = false;
    emit scaningStateChanged();
}

void BluetoothDeviceModel::deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    if (error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
        qWarning() << "The Bluetooth adaptor is powered off, power it on before doing discovery.";
    else if (error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
        qWarning() << "Writing or reading from the device resulted in an error";
    else {
        static QMetaEnum qme = m_deviceDiscoveryAgent->metaObject()->enumerator(
            m_deviceDiscoveryAgent->metaObject()->indexOfEnumerator("Error"));
        qWarning() << "Error: " + QLatin1String(qme.valueToKey(error));
    }

    m_scaningState = false;
    emit scaningStateChanged();
}

void BluetoothDeviceModel::cancelDeviceScan()
{
    if (m_deviceDiscoveryAgent->isActive()) {
        m_deviceDiscoveryAgent->stop();
    }
}
