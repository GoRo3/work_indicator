#ifndef BTDEVICEMANAGER_HPP
#define BTDEVICEMANAGER_HPP

#include <QObject>

#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QVector>

#include "btindicator.hpp"
#include "ledservicestate.hpp"
#include "serializer.hpp"

class BtDeviceManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connectedState READ connectedState NOTIFY connectedStateChanged)
public:
    enum class AddressType {
        PublicAddress,
        RandomAddress
    };
    Q_ENUM(AddressType)

public:
    explicit BtDeviceManager(QObject *parent = nullptr);
    void ledWriteCompleted();

public slots:
    void connectToDevice(QVariant a_variant);
    void disconnectFromDevice();
    void setState(LedServiceState::LedState_t a_state);
    bool connectedState() const;

signals:
    void connectionError();
    void connectedStateChanged();

private:
    void serviceDiscovered(const QBluetoothUuid &gatt);
    void serviceScanDone();
    void connectBt();

private:
    BtIndicator* m_device{};
    std::vector<QLowEnergyService*> m_tempServices{};
    QLowEnergyController *m_connectionController = nullptr;
    bool m_connectedState{ false };
    Serializer m_serializer;

};

#endif // BTDEVICEMANAGER_HPP
