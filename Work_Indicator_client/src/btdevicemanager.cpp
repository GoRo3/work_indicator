#include "btdevicemanager.hpp"

#include <memory>

#include <QBluetoothDeviceInfo>
#include <QLowEnergyService>

#include <QDebug>
#include <QVariant>
#include <functional>

#include "btdevice.hpp"
#include "btindicator.hpp"
#include "ledservice.hpp"
#include "fileserializeservice.hpp"

using namespace std::placeholders;

BtDeviceManager::BtDeviceManager(QObject *parent) : QObject(parent),
                                                    m_serializer(std::make_unique<FileSerializeService>())
{
    auto device = m_serializer.deserialize();
    if (device) {
        m_device = device;
        m_device->setManager(this);

        connectBt();
    }
}

void BtDeviceManager::setState(LedServiceState::LedState_t a_state)
{
    LedServiceState state(a_state);

    if(!m_device) {
        qWarning() << "State unable to set. Device not connected";
        return;
    }

    m_device->setState(state);
}

void BtDeviceManager::connectToDevice(QVariant a_variant) {

    if( !(m_device = dynamic_cast<BtIndicator*>(a_variant.value<BtDevice*>())) ) {
        emit connectionError();
        return;
    }

    m_device->setManager(this);

    connectBt();
}

void BtDeviceManager::disconnectFromDevice()
{
    qDebug() << "Disconnecr from device invoked";

    m_connectionController->disconnectFromDevice();
    m_tempServices.clear();
    m_device->disconnected();
    // Remeber to remove all characteristics, and services from memory.
}

void BtDeviceManager::serviceDiscovered(const QBluetoothUuid &gatt)
{
    auto temp_service = m_connectionController->createServiceObject(gatt, this);

    if (!temp_service) {
        qWarning() << "Canot create service for this uuid";
        emit connectionError();
        return;
    }
    m_tempServices.push_back(temp_service);
}

void BtDeviceManager::serviceScanDone()
{
    qDebug() << "Service scan Done";

    for (auto *service : m_tempServices) {
        if (service->serviceUuid() == QBluetoothUuid(QString(IndicatorServices::LED_SERVICE))) {
            m_device->setLedService(std::make_unique<LedService>(service));
        } else {
            qDebug() << "Uknown Service" << service->serviceUuid();
        }
    }
}

void BtDeviceManager::connectBt()
{
    if (m_connectionController) {
        m_connectionController->disconnectFromDevice();
        delete m_connectionController;
        m_connectionController = nullptr;
    }

    m_connectionController = QLowEnergyController::createCentral(m_device->info(), this);

    connect(m_connectionController, &QLowEnergyController::serviceDiscovered,
            this, &BtDeviceManager::serviceDiscovered);
    connect(m_connectionController, &QLowEnergyController::discoveryFinished,
            this, &BtDeviceManager::serviceScanDone);

    connect(m_connectionController, static_cast<void (QLowEnergyController::*)(QLowEnergyController::Error)>(&QLowEnergyController::error),
            [&](QLowEnergyController::Error error) {
                Q_UNUSED(error);
                qWarning() << "Cannot connect to remote device.";
            });
    connect(m_connectionController, &QLowEnergyController::connected, [&]() {
        qDebug() << "Controller connected. Search services...";
        m_connectedState = true;
        emit connectedStateChanged();
        m_connectionController->discoverServices();
    });
    connect(m_connectionController, &QLowEnergyController::disconnected, [&]() {
        qDebug() << "LowEnergy controller disconnected";
        m_connectedState = false;
        emit connectedStateChanged();
    });

    // Connect
    m_connectionController->connectToDevice();
}

void BtDeviceManager::ledWriteCompleted()
{
    if (!m_serializer.serialize(m_device)) {
        qWarning() << "Error in serializing device";
    }
}

bool BtDeviceManager::connectedState() const
{
    return m_connectedState;
}
