#ifndef BTINDICATOR_HPP
#define BTINDICATOR_HPP

#include <QMetaType>

#include <memory>
#include <functional>

#include <QLowEnergyService>

#include "btdevice.hpp"
#include "ledservice.hpp"

namespace IndicatorServices {
    static constexpr const char LED_SERVICE[] = "5db9675a-9e25-9694-3343-ec1fb0480bd4";
}

class BtDeviceManager;

class BtIndicator: public BtDevice
{
public:
    //ToDO (GoRo3) Change led Service from smart pointer to normal pointer. 
    using ledService_t = std::unique_ptr<LedService>;

public:
    BtIndicator(const QBluetoothDeviceInfo a_info);
    ~BtIndicator() override = default;

    void setLedService(ledService_t a_ledService);
    void setState(LedServiceState a_state);
    void saveDavice(QJsonObject &a_data) override;
    void disconnected() override;

    void setManager(BtDeviceManager *a_manager);
private:
    ledService_t m_ledService{};
    BtDeviceManager *m_manager = nullptr;

    // BtDevice interface
};

Q_DECLARE_METATYPE(BtIndicator*);

#endif // BTINDYCATOR_HPP
