#ifndef FILESERIALZESERVICE_HPP
#define FILESERIALZESERVICE_HPP

#include "serializeservice.hpp"

#include <QFile>
#include <QJsonObject>

static constexpr const char DATA_FILE_NAME[] = "Data.bin";

class FileSerializeService : public SerializeService
{
public:
    FileSerializeService();
    ~FileSerializeService() override = default;

    QJsonObject read() override;
    qint64 write(const QJsonObject &a_data) override;

private:
    QFile m_file{};
};

#endif // FILESERIALZESERVICE_HPP
