#include "serializer.hpp"

#include <QJsonObject>
#include <QBluetoothDeviceInfo>
#include <QBluetoothUuid>

#include "ledservicestate.hpp"
#include "btindicator.hpp"


Serializer::Serializer(SerializeService_p a_serializeService) :
             m_serializeService(std::move(a_serializeService))
{

}

bool Serializer::serialize(BtIndicator *a_device)
{
    if (!a_device) {
        return false;
    }

    QJsonObject serializeObjcet;

    a_device->saveDavice(serializeObjcet);

    if (0 < m_serializeService->write(serializeObjcet)) {
        return true;
    }

    return false;
}

BtIndicator *Serializer::deserialize()
{
    QJsonObject readedData;

    readedData = m_serializeService->read();
    if (readedData.isEmpty()) {
        return nullptr;
    }

    QString uuid;
    if (readedData.contains("uuid") && readedData["uuid"].isString()) {
        uuid = readedData["uuid"].toString();
    }

    QString name;
    if (readedData.contains("name") && readedData["name"].isString()) {
        name = readedData["name"].toString();
    }

    int deviceClass = 0;
    if (readedData.contains("deviceClass") && readedData["deviceClass"].isDouble()) {
        deviceClass = readedData["deviceClass"].toInt();
    }

    LedServiceState state;
    if (readedData.contains("state") && readedData["state"].isDouble()) {
        state.setState(readedData["state"].toInt());
    }

    QBluetoothDeviceInfo info(QBluetoothUuid(uuid), name, deviceClass);

    auto indicator = new BtIndicator(info);

    return indicator;
}
