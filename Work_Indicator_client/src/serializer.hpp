#ifndef SERIALIZER_HPP
#define SERIALIZER_HPP

#include <memory>

#include "btindicator.hpp"
#include "serializeservice.hpp"

class Serializer
{
public:
    using SerializeService_p = std::unique_ptr<SerializeService>;
public:
    Serializer(SerializeService_p a_serializeService);
    bool serialize(BtIndicator *a_device);
    BtIndicator *deserialize();

private:
    SerializeService_p m_serializeService;
};

#endif // SERIALIZER_HPP
