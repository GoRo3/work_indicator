#include <QtWidgets/QApplication>


#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QIcon>
#include <QMetaType>
#include <QDir>
#include <QDebug>

#include <memory>

#include "bluetoothdevicemodel.hpp"
#include "btdevicemanager.hpp"
#include "btdevice.hpp"
#include "ledservicestate.hpp"

void setCurrentPath();

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);


    QApplication app(argc, argv);

    app.setWindowIcon(QIcon(":/images/led-light.png"));

    setCurrentPath();

    BtDeviceManager *btManager = new BtDeviceManager();

    if (!btManager) {
        qFatal("Error creating BtDeviceManager");
    }

    qRegisterMetaType<LedServiceState::LedState_t>("LedService");
    qmlRegisterUncreatableType<LedServiceState>("BluetoothDevices", 1, 0, "LedService", "LedService Should not be created in QML");
    qmlRegisterUncreatableType<BtDeviceManager>("BluetoothDevices", 1, 0, "BluetoothDeviceManager","BluetoothDeviceManager Should not be created in QML");
    qmlRegisterType<BluetoothDeviceModel>("BluetoothDevices", 1, 0, "BluetoothDeviceModel");
    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("btDeviceManager", btManager);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

void setCurrentPath() {
    QString currentPath = QDir::homePath() + "/.BusyIndicator/";

    QDir newPath(currentPath);
    if (!newPath.exists()) {
        if (!newPath.mkdir(currentPath)) {
            qWarning() << "Unable to construct application path";
        };
    }

    if(!QDir::setCurrent(newPath.path())) {
        qWarning() << "Problem with setting application current path";
    }
}
