#include "btdevice.hpp"

#include <memory>
#include <cstring>
#include <QBluetoothAddress>
#include <QBluetoothUuid>
#include <QJsonObject>

struct classofDevice_t {
    qint8 unused : 2;
    qint8 miniordeviceClass : 6;
    qint8 majorDeviceClass : 5;
    qint16 serviceClass : 11;
};

BtDevice::BtDevice()
{

}

BtDevice::BtDevice(const QBluetoothDeviceInfo a_info) :
                                                        m_name(a_info.name()),
                                                        m_info(std::move(a_info))

{

}

void BtDevice::disconnected()
{}

void BtDevice::saveDavice(QJsonObject &a_data)
{
    a_data["uuid"] = deviceUuid();
    a_data["deviceClass"] = deviceClass();
    a_data["name"] = deviceName();
}

QString BtDevice::deviceAddress() const
{
#if defined Q_OS_DARWIN
    // workaround for Core Bluetooth:
    return m_info.deviceUuid().toString();
#else
    return m_info.address().toString();
#endif
}

QString BtDevice::deviceName() const
{
    return m_name;
}

QString BtDevice::deviceUuid() const
{
    return m_info.deviceUuid().toString();
}

qint16 BtDevice::deviceRssi() const
{
    return m_info.rssi();
}

qint32 BtDevice::deviceClass() const
{
    classofDevice_t classOfdevice;
    classOfdevice.unused = 0;
    classOfdevice.miniordeviceClass = m_info.minorDeviceClass();
    classOfdevice.majorDeviceClass = m_info.majorDeviceClass();
    classOfdevice.serviceClass = m_info.serviceClasses();

    quint32 classDevicesEncoded = 0;
    std::memcpy(&classDevicesEncoded, &classOfdevice, sizeof (classOfdevice));

    return classDevicesEncoded;
}

const QBluetoothDeviceInfo& BtDevice::info() const
{
    return m_info;
}

bool BtDevice::isCompatible() const
{
    return m_isCompatible;
}
