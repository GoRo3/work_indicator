#ifndef BLUETOOTHDEVICEMODEL_HPP
#define BLUETOOTHDEVICEMODEL_HPP

#include <QObject>
#include <QAbstractListModel>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>

#include <vector>
#include <mutex>

#include "btdevice.hpp"

class BluetoothDeviceModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(BluetoothDeviceModel)
    Q_PROPERTY(bool scaningState READ scaningState WRITE setScaningState NOTIFY scaningStateChanged)

public:
    explicit BluetoothDeviceModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool scaningState() const;
    void setScaningState(bool scaningState);

signals:
    void scaningStateChanged();

public slots:
    void scanDevices();
    void cancelDeviceScan();

private slots:
    void addDeviceToList(const QBluetoothDeviceInfo &a_info);
    void scanFinished();
    void deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error);

private:
    void scanForBtDevices();
    bool checkIfDeviceisKnown(const QBluetoothDeviceInfo &a_info);

private:
    std::vector<BtDevice *> m_devices{};
    QBluetoothDeviceDiscoveryAgent *m_deviceDiscoveryAgent{};
    bool m_scaningState{ false };
    std::mutex m_devicesLocker {};

private:
    enum roleNames {
        E_DeviceAddres,
        E_DeviceName,
        E_DeviceUUID,
        E_DeviceRssi,
        E_Device,
        E_DeviceIsCompatible
    };
};

#endif // BLUETOOTHDEVICEMODEL_HPP
