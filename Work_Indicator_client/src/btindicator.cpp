﻿#include "btindicator.hpp"

#include "QObject"

#include <QBluetoothAddress>
#include <QBluetoothUuid>
#include <QString>
#include <QJsonObject>

#include "btdevicemanager.hpp"
#include "btdevice.hpp"

static constexpr const char* KNOWN_NAME = "HO_LED";

BtIndicator::BtIndicator(const QBluetoothDeviceInfo a_info) :
                                                                                             BtDevice(a_info)
{
    // TODO (GoRo3) Check why wy are loosing name of device after scan complete signal is emited;
    if(a_info.name().isEmpty()) {
        m_name = KNOWN_NAME;
    }

    m_isCompatible = true;
}

void BtIndicator::setLedService(BtIndicator::ledService_t a_ledService)
{
    m_ledService = std::move(a_ledService);

    QObject::connect(m_ledService.get(), &LedService::ledReady, [&](){
        m_ledService->readLedState();
    });
    QObject::connect(m_ledService.get(), &LedService::ledWriteSuccessful, [&](){
        if (m_manager) {
            m_manager->ledWriteCompleted();
        }
    });
}

void BtIndicator::setState(LedServiceState a_state)
{
    m_ledService->setLedState(a_state);
}

void BtIndicator::saveDavice(QJsonObject &a_data)
{
    BtDevice::saveDavice(a_data);

    a_data["state"] = m_ledService->getLedState().getStateInt();
}

void BtIndicator::disconnected()
{
    m_ledService.reset(nullptr);
    m_manager = nullptr;
}

void BtIndicator::setManager(BtDeviceManager *a_manager)
{
    if (a_manager) {
        m_manager = a_manager;
    }
}
