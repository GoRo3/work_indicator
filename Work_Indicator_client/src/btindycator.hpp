#ifndef BTINDICATOR_HPP
#define BTINDICATOR_HPP

#include "btdevice.hpp"

class BtIndicator: public BtDevice
{
public:
    explicit BtIndicator(const QBluetoothDeviceInfo &a_info);
};

#endif // BTINDYCATOR_HPP
