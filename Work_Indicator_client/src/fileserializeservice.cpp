#include "fileserializeservice.hpp"

#include <QFile>
#include <QDir>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>

#include <stdexcept>

FileSerializeService::FileSerializeService() : m_file(DATA_FILE_NAME)
{
    if (!m_file.open(QIODevice::ReadWrite)) {
        qWarning() << "Unable to open data file. Some Error: " << m_file.errorString();
        throw std::runtime_error("Error durning opening data file");
    }
}

QJsonObject FileSerializeService::read()
{
    QByteArray loadedData = m_file.readAll();
    if(loadedData.size() == 0) {
        return {};
    }

    QJsonDocument loadDoc(QJsonDocument::fromBinaryData(loadedData));

    return loadDoc.object();
}

qint64 FileSerializeService::write(const QJsonObject &a_data)
{
    if (!m_file.reset()) {
        qWarning() << __FUNCTION__ <<"Error durning file operation";
    }

    QJsonDocument saveDoc(a_data);
    qint64 writed = 0;
    writed = m_file.write(saveDoc.toBinaryData());
    if (writed == -1) {
        qWarning() << __FUNCTION__ << "Error durning save data to file";
        return 0;
    }

    m_file.flush();
    return writed;
}
