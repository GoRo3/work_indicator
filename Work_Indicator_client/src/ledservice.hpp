﻿#ifndef LEDSERVICE_HPP
#define LEDSERVICE_HPP

#include <QObject>

#include <memory>

#include <QLowEnergyService>
#include <QScopedPointer>
#include <QLowEnergyCharacteristic>
#include <QList>

#include "ledservicestate.hpp"


namespace LedCharacteristicsUUID {
    static constexpr const char LED_SETUP[] = "019bb9c0-a4d9-1b83-014b-4c60773593a6";
}

class LedService : public QObject
{
    Q_OBJECT
public:
    explicit LedService(QLowEnergyService *a_service, QObject *a_object = nullptr);

    void readLedState();
    void setLedState(LedServiceState a_state);
    LedServiceState getLedState() const;

public slots:
   void serviceDetailsDiscovered(QLowEnergyService::ServiceState newState);
   void error(QLowEnergyService::ServiceError newError);

signals:
       void ledReady();
       void ledStatusUpdated();
       void ledError();
       void ledWriteSuccessful();

private:
       void configureLedCharacteristic();

private:
    LedServiceState m_ledState;
    QScopedPointer<QLowEnergyService> m_service {nullptr};
    QLowEnergyCharacteristic m_ledCharacteristic{};
    QBluetoothUuid m_ledUUID{};
};

#endif // LEDSERVICE_HPP
