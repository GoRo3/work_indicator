#include "ledservicestate.hpp"

#include <QByteArray>
#include <QDebug>
#include <QString>

LedServiceState::LedServiceState(LedState_t a_state): m_state(a_state)
{
}

LedServiceState::operator QString() const
{
    switch (m_state) {
    case LedServiceState::LedState_t::E_LED_STATE_OFF: {
        return { "Led state: OFF" };
    }
    case LedServiceState::LedState_t::E_LED_STATE_GREEN: {
        return {"Led state: Green"};
    }
    case LedServiceState::LedState_t::E_LED_STATE_RED: {
        return {"Led state: Red"};
    }
    case LedServiceState::LedState_t::E_LED_STATE_BLUE: {
        return {"Led state: Blue"};
    }
    case LedServiceState::LedState_t::E_LED_STATE_YELLOW: {
        return {"Led state: Yellow"};
    }
    case LedServiceState::LedState_t::E_LED_STATE_ERR: {
        return {"Led state: Error"};
    }
    case LedServiceState::LedState_t::E_LED_STATE_MAX: {
        return{"Led state: Max number of led's state"};
    }
    }
}

QByteArray LedServiceState::getState() const
{
    uint8_t value = static_cast<uint8_t>(m_state);

    QByteArray array;
    array.push_back(value);
    return  array;
}

int LedServiceState::getStateInt() const
{
    return static_cast<int>(m_state);
}

bool LedServiceState::setState(const QByteArray &a_arrayState)
{
    if (a_arrayState.size() > 1) {
        qWarning() << __FUNCTION__ << ": Unable to convert a_arrayState to Led Service state. Container to big";
        return false;
    }

    uint8_t state = a_arrayState[0];

    if (state >= static_cast<uint8_t>(LedState_t::E_LED_STATE_MAX)) {
        qWarning() << __FUNCTION__ << ": Bad value of Led Service state returned";
        return false;
    }

    m_state = static_cast<LedState_t>(state);
    return true;
}

bool LedServiceState::setState(int a_state)
{
    if (a_state >= static_cast<int>(LedState_t::E_LED_STATE_MAX)) {
        m_state = LedState_t::E_LED_STATE_ERR;
        return false;
    }

    m_state = static_cast<LedState_t>(a_state);
    return true;
}

