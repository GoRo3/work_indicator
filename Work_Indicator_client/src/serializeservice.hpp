#ifndef SERIALIZESERVICE_HPP
#define SERIALIZESERVICE_HPP

#include <QJsonObject>

class SerializeService
{
public:
    virtual ~SerializeService() {};

    virtual QJsonObject read() = 0;
    virtual qint64 write(const QJsonObject &a_data) = 0;
protected:
    SerializeService() {};
};
#endif // SERIALIZESERVICE_HPP
