#ifndef BLUETOOTHDEVICE_H
#define BLUETOOTHDEVICE_H

#include <QObject>

class BluetoothDevice : public QObject
{
    Q_OBJECT

  // Q_PROPERTY(QString deviceAddress READ deviceAddress WRITE setDeviceAddress NOTIFY deviceAddressChanged)
  // Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
public:
    BluetoothDevice(QString a_name, QString a_address, QObject *parent = nullptr);
    explicit BluetoothDevice(QObject *parent = nullptr);

    QString deviceName() const;
    void setDeviceName(const QString &deviceName);

    QString deviceAddress() const;
    void setDeviceAddress(const QString &deviceAddress);

signals:
//    void deviceAddressChanged();
//    void deviceNameChanged();

private:
    QString m_deviceAddress{};
    QString m_deviceName{};

};

#endif // BLUETOOTHDEVICE_H
