#ifndef LEDSERVICESTATE_HPP
#define LEDSERVICESTATE_HPP

#include <QObject>

#include <QByteArray>
#include <QString>

#include <iostream>

class LedServiceState
{
  Q_GADGET

public:
    enum class LedState_t : quint8 {
        E_LED_STATE_OFF,
        E_LED_STATE_GREEN,
        E_LED_STATE_RED,
        E_LED_STATE_BLUE,
        E_LED_STATE_YELLOW,
        E_LED_STATE_MAX,
        E_LED_STATE_ERR
    };
    Q_ENUM(LedState_t)

public:
    LedServiceState() = default;
    explicit LedServiceState(LedState_t a_state);

    operator QString() const;

    bool operator==(const LedServiceState &rhs) {
        return  (m_state == rhs.m_state);
    }

    bool operator!=(const LedServiceState &rhs) {
        return  !(m_state == rhs.m_state);
    }

    QByteArray getState() const;
    int getStateInt() const;

    bool setState(const QByteArray &a_arrayState);
    bool setState(int a_state);

private:
    LedState_t m_state;
};


#endif // LEDSERVICESTATE_HPP
