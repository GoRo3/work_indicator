﻿#include "ledservice.hpp"

#include <QDebug>
#include <QLowEnergyCharacteristic>
#include <QLowEnergyService>
#include <QBluetoothUuid>
#include <QByteArray>

#include "ledservicestate.hpp"

LedService::LedService(QLowEnergyService *a_service, QObject *a_object) :
                                                                          QObject(a_object),
                                                                          m_ledState(LedServiceState::LedState_t::E_LED_STATE_OFF),
                                                                          m_service(a_service),
                                                                          m_ledUUID(QString(LedCharacteristicsUUID::LED_SETUP))
{
    m_service->setParent(this);

    connect(m_service.data(), QOverload<QLowEnergyService::ServiceError>::of(&QLowEnergyService::error), this, &LedService::error);
    connect(m_service.data(), &QLowEnergyService::characteristicWritten, [&](const QLowEnergyCharacteristic &characteristic, const QByteArray &newValue){
        if (m_ledCharacteristic == characteristic) {
            m_ledState.setState(newValue);
            emit ledWriteSuccessful();
        }
    });
    connect(m_service.data(), &QLowEnergyService::characteristicRead, [&](const QLowEnergyCharacteristic &info, const  QByteArray &value){
        if (info.uuid() == m_ledUUID) {
           m_ledState.setState(value);
           emit ledStatusUpdated();
        }
    });

    if (m_service->state() == QLowEnergyService::DiscoveryRequired) {
        connect(m_service.data(), &QLowEnergyService::stateChanged, this, &LedService::serviceDetailsDiscovered);
        m_service->discoverDetails();
    } else {
        configureLedCharacteristic();
    }
}

void LedService::readLedState()
{
    m_service->readCharacteristic(m_ledCharacteristic);
}

void LedService::setLedState(LedServiceState a_state)
{
    if (a_state != m_ledState) {
        m_service->writeCharacteristic(m_ledCharacteristic, a_state.getState());
    }
}

LedServiceState LedService::getLedState() const
{
    return m_ledState;
}

void LedService::serviceDetailsDiscovered(QLowEnergyService::ServiceState newState)
{
    if (newState != QLowEnergyService::ServiceDiscovered) {
        if (newState != QLowEnergyService::DiscoveringServices) {
            QMetaObject::invokeMethod(this, "characteristicsUpdated",
                                      Qt::QueuedConnection);
        }
        return;
    }

    configureLedCharacteristic();
}

void LedService::error(QLowEnergyService::ServiceError newError)
{
    switch (newError) {
    case QLowEnergyService::NoError: {
        return;
    }
    case QLowEnergyService::CharacteristicReadError: {
        qWarning() << __FUNCTION__ << ": Characteristin Read Error";
        break;
    }
    case QLowEnergyService::CharacteristicWriteError: {
        qWarning() << __FUNCTION__ << ": Characteristic Write Error in LedService";
        break;
    }
    case QLowEnergyService::UnknownError: {
        qWarning() << __FUNCTION__ << ": Unknown Error in LedService";
        break;
    }
    default: {
        qWarning() << __FUNCTION__ << ": Other Error in LedService";
        break;
    }
    }
    emit ledError();
}

void LedService::configureLedCharacteristic()
{
    m_ledCharacteristic = m_service->characteristic(m_ledUUID);
    if (!m_ledCharacteristic.isValid()) {
        qWarning() << __FUNCTION__ << ": Invalid setup of led characteristic";
        return;
    }

    emit ledReady();
}
