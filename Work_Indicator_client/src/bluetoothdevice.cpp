#include "bluetoothdevice.hpp"

BluetoothDevice::BluetoothDevice(QString a_name, QString a_address, QObject *parent) :
                                                                                       QObject(parent),
                                                                                       m_deviceAddress(std::move(a_address)),
                                                                                       m_deviceName(std::move(a_name))

{

}

BluetoothDevice::BluetoothDevice(QObject *parent) : QObject(parent)
{

}

QString BluetoothDevice::deviceName() const
{
    return m_deviceName;
}

void BluetoothDevice::setDeviceName(const QString &deviceName)
{
    if (m_deviceName == deviceName) {
        return;
    }

    m_deviceName = deviceName;
//    emit deviceNameChanged();
}

QString BluetoothDevice::deviceAddress() const
{
    return m_deviceAddress;
}

void BluetoothDevice::setDeviceAddress(const QString &deviceAddress)
{
    if (m_deviceAddress == deviceAddress) {
        return;
    }

    m_deviceAddress = deviceAddress;
//    emit deviceAddressChanged();
}

#include bluetoothdevice.moc
