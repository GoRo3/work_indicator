#ifndef BTDEVICE_HPP
#define BTDEVICE_HPP

#include <QMetaType>
#include <QBluetoothDeviceInfo>
#include <QString>

class BtDevice
{
public:
    BtDevice();
    explicit BtDevice(const QBluetoothDeviceInfo a_info);
    virtual ~BtDevice() = default;

    virtual void disconnected();
    virtual void saveDavice(QJsonObject &a_data);


    QString deviceAddress() const;
    QString deviceName() const;
    QString deviceUuid() const;
    qint16 deviceRssi() const;
    qint32 deviceClass() const;

    const QBluetoothDeviceInfo& info() const;
    bool isCompatible() const;

protected:
        bool m_isCompatible{ false };
        QString m_name{};

private:
    QBluetoothDeviceInfo m_info{};
};

Q_DECLARE_METATYPE(BtDevice*);

#endif // BTDEVICE_HPP
