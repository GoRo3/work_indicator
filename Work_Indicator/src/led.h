#ifndef LED_H
#define LED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "freertos/FreeRTOS.h"

bool led_init();
void led_change_state(int state);
uint8_t get_led_state();

#ifdef __cplusplus
}
#endif
#endif // LED_H
