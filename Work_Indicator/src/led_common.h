#ifndef LED_COMMON_H
#define LED_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum {
    E_LED_STATE_OFF,
    E_LED_STATE_GREEN,
    E_LED_STATE_RED,
    E_LED_STATE_BLUE,
    E_LED_STATE_YELLOW,
    E_LED_STATE_MAX,
    E_LED_STATE_ERR
} led_states_t; 


led_states_t convert_state_from_int(int number);
uint8_t convert_state_to_int(led_states_t a_led_state);

#ifdef __cplusplus
}
#endif
#endif // LED_COMMON_H