#include "led.h"

#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"

#include "led_common.h"

#define LED_TAG "LED'S Handler"
#define DB_LED_KEY "LED_STATE"

#define LED_GREEN_CH 19
#define LED_RED_CH 5
#define LED_BLUE_CH 18

#define LEDC_DUTY         (4000)
#define LEDC_FADE_TIME    (3000)

static ledc_channel_config_t led_green_cfg = {
            .channel    = LEDC_CHANNEL_0,
            .duty       = 0,
            .gpio_num   = LED_GREEN_CH,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0
};
static ledc_channel_config_t led_red_cfg = {
            .channel    = LEDC_CHANNEL_1,
            .duty       = 0,
            .gpio_num   = LED_RED_CH,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0
};
static ledc_channel_config_t led_blue_cfg = {
            .channel    = LEDC_CHANNEL_2,
            .duty       = 0,
            .gpio_num   = LED_BLUE_CH,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0
};

static led_states_t led_current_state = E_LED_STATE_OFF;
nvs_handle_t db_handle;  

static ledc_channel_config_t* led_state_to_config(led_states_t state); 
static void deinit_last_state();
static void init_new_state(led_states_t new_state);
static void init_db();
static void read_from_db();
static void write_to_db(uint8_t led_value);

bool led_init() 
{
    ledc_timer_config_t timer_cfg = {
        .duty_resolution = LEDC_TIMER_13_BIT,
        .freq_hz = 5000,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .timer_num = LEDC_TIMER_0,
        .clk_cfg = LEDC_AUTO_CLK,
    };

    ledc_timer_config(&timer_cfg);

    ledc_channel_config(&led_red_cfg);
    ledc_channel_config(&led_blue_cfg);
    ledc_channel_config(&led_green_cfg);

    ledc_fade_func_install(0);
    
    init_db();

    init_new_state(led_current_state);

    return true;
}

void led_change_state(int state) 
{
    led_states_t new_state = convert_state_from_int(state); 
    if (E_LED_STATE_ERR == new_state) {
        ESP_LOGE(LED_TAG, "ERROR wrong led state. Number: %d \n", new_state);
        return;
    }

    if (led_current_state == new_state) {
        return;
    }

    deinit_last_state();
    vTaskDelay(LEDC_FADE_TIME / portTICK_PERIOD_MS);
    init_new_state(new_state);
    
    write_to_db(new_state);
    led_current_state = new_state;
}

uint8_t get_led_state() 
{
    return convert_state_to_int(led_current_state);
}

void deinit_last_state() 
{
    if (led_current_state == E_LED_STATE_OFF) {
        return;
    } else if (led_current_state == E_LED_STATE_YELLOW) {
        ledc_set_fade_with_time(led_red_cfg.speed_mode,
                    led_red_cfg.channel, 0, LEDC_FADE_TIME);
        ledc_fade_start(led_red_cfg.speed_mode,
                    led_red_cfg.channel, LEDC_FADE_NO_WAIT);
        ledc_set_fade_with_time(led_green_cfg.speed_mode,
                    led_green_cfg.channel, 0, LEDC_FADE_TIME);
        ledc_fade_start(led_green_cfg.speed_mode,
                    led_green_cfg.channel, LEDC_FADE_NO_WAIT);
        return;
    }

    ledc_channel_config_t* led_cfg = led_state_to_config(led_current_state);
    if (led_cfg == NULL) {
        return;
    }

    ledc_set_fade_with_time(led_cfg->speed_mode,
                    led_cfg->channel, 0, LEDC_FADE_TIME);
    ledc_fade_start(led_cfg->speed_mode,
                    led_cfg->channel, LEDC_FADE_NO_WAIT);
}
void init_new_state(led_states_t new_state) 
{
    if (new_state == E_LED_STATE_OFF) {
        return;
    } else if (new_state == E_LED_STATE_YELLOW) {
        ledc_set_fade_with_time(led_red_cfg.speed_mode,
                    led_red_cfg.channel, LEDC_DUTY, LEDC_FADE_TIME);
        ledc_fade_start(led_red_cfg.speed_mode,
                    led_red_cfg.channel, LEDC_FADE_NO_WAIT);
        ledc_set_fade_with_time(led_green_cfg.speed_mode,
                    led_green_cfg.channel, LEDC_DUTY, LEDC_FADE_TIME);
        ledc_fade_start(led_green_cfg.speed_mode,
                    led_green_cfg.channel, LEDC_FADE_NO_WAIT);
        return;
    }

    ledc_channel_config_t* led_cfg = led_state_to_config(new_state);
    if (led_cfg == NULL) {
        return;
    }

    ledc_set_fade_with_time(led_cfg->speed_mode,
                    led_cfg->channel, LEDC_DUTY, LEDC_FADE_TIME);
    ledc_fade_start(led_cfg->speed_mode,
                    led_cfg->channel, LEDC_FADE_NO_WAIT);
}

ledc_channel_config_t* led_state_to_config(led_states_t state) 
{
    switch (state)
    {
    case E_LED_STATE_GREEN:
        return &led_green_cfg;
    case E_LED_STATE_BLUE:
        return &led_blue_cfg;
    case E_LED_STATE_RED:
        return &led_red_cfg;
    default: {
        ESP_LOGE(LED_TAG, "Unknow state to convert");
        return NULL;
        }
    }
}
void init_db() {
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &db_handle);
    if (err != ESP_OK) {
        ESP_LOGE(LED_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return;
    }

    read_from_db();
}

void read_from_db() 
{   
    int8_t temp_value = 0;
    esp_err_t err = nvs_get_i8(db_handle, DB_LED_KEY, &temp_value);
    switch (err) {
            case ESP_OK:
                led_current_state = convert_state_from_int(temp_value);
                ESP_LOGI(LED_TAG, "Restoring LED Database Done\n");
                break;
            case ESP_ERR_NVS_NOT_FOUND:
                ESP_LOGW(LED_TAG, "The value is not initialized yet!\n");
                break;
            default :
                ESP_LOGE(LED_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
        }
}

void write_to_db(uint8_t led_value) 
{
    if (led_current_state == led_value) {
        return;
    }

    esp_err_t err = nvs_set_i8(db_handle, DB_LED_KEY, led_value);
    switch (err) {
            case ESP_OK:
                ESP_LOGI(LED_TAG, "Save led state Done\n");
                return;
            default :
                ESP_LOGE(LED_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
        }

    err = nvs_commit(db_handle);
    if (err != ESP_OK) {
        ESP_LOGE(LED_TAG, "Error (%s) durning commit to DB!\n", esp_err_to_name(err));
    }
}
