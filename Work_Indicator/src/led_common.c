#include "led_common.h"

#include <stdio.h>
#include <stdint.h>

led_states_t convert_state_from_int(int number) {
    if ((number >= E_LED_STATE_MAX) || (0 > number)) {
        return E_LED_STATE_ERR;
    }

    led_states_t led_state = (led_states_t)number;
    return led_state; 
}

uint8_t convert_state_to_int(led_states_t a_led_state) {
    return (uint8_t)a_led_state;
}